module gitlab.com/kskewes/palace-advertising-go

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20200428143746-21a406dcc535 // indirect
	github.com/go-openapi/errors v0.19.4
	github.com/go-openapi/loads v0.19.5 // indirect
	github.com/go-openapi/runtime v0.19.15
	github.com/go-openapi/spec v0.19.8 // indirect
	github.com/go-openapi/strfmt v0.19.5
	github.com/go-openapi/swag v0.19.9
	github.com/go-openapi/validate v0.19.8 // indirect
	github.com/mitchellh/mapstructure v1.3.1 // indirect
	go.mongodb.org/mongo-driver v1.3.3 // indirect
	golang.org/x/net v0.0.0-20200520182314-0ba52f642ac2 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
