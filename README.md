# Palace Advertising Go

[![build](https://gitlab.com/kskewes/palace-advertising-go/badges/master/pipeline.svg)](https://gitlab.com/kskewes/palace-advertising-go)

`palace-advertising-go` is an unofficial Go client library for accessing the [Palace Advertising Integration API](https://app.swaggerhub.com/apis-docs/Palace/Advertising_Integration/2.0.0#/)

## Installation

Install palace-advertising-go with:

```sh
go get -u gitlab.com/kskewes/palace-advertising-go
```

Then import it using:
```go
import (
	apiclient "gitlab.com/kskewes/palace-advertising-go/client"
)
```

See [examples](./examples).

## Documentation

## Contribution

PR's welcome, please create an issue first.

## Test

WIP
